<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
       aria-expanded="false">Channels <span class="caret"></span></a>

    <ul class="dropdown-menu">
        @foreach (App\Channel::all() as $channel)
            <li><a href="/threads/{{ $channel->slug }}">{{ $channel->name }}</a></li>
        @endforeach
    </ul>
</li>
