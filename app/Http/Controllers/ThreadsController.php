<?php
namespace App\Http\Controllers;
use App\Channel;
use App\Thread;
use App\Filters\ThreadFilters;
use App\Filters\Filters;

use Illuminate\Http\Request;
class ThreadsController extends Controller
{
    /**
     * ThreadsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index(Channel $channel,ThreadFilters $filters)
    {
        $threads = Thread::latest();

        if($channel->exists) {
            $threads->where('channel_id', $channel->id);
        }

        $threads = $threads->filter($filters)->get();
        return view('threads.index', compact('threads'));
    }

    public function create()
    {
        return view('threads.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'channel_id' => 'required|exists:channels,id'
        ]);
        $thread = Thread::create([
            'user_id' => auth()->id(),
            'channel_id' => request('channel_id'),
            'title' => request('title'),
            'body' => request('body')
        ]);
        return redirect($thread->path());
    }

    public function show($channelId, Thread $thread)
    {
        return view('threads.show', compact('thread'));
    }

    public function edit(Thread $thread)
    {
        //
    }

    public function update(Request $request, Thread $thread)
    {
        //
    }

    public function destroy(Thread $thread)
    {
        //
    }

//    /**
//     * @param Channel $channel
//     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection|static
//     */
//    protected function getThreads(Channel $channel)
//    {
//        if ($channel->exists) {
//            $threads = $channel->threads()->latest();
//        } else {
//            $threads = Thread::latest();
//        }
//
//        if ($username = request('by')) {
//            $user = \App\User::where('name', $username)->firstOrFail();
//
//            $threads->where('user_id', $user->id);
//        }
//
//        $threads = $threads->get();
//        return $threads;
//    }
}
