<?php

namespace Tests\Feature;

use Tests\TestCase;
 use Illuminate\Foundation\Testing\DatabaseMigrations;

class ParticipateInForumTest extends TestCase
{
     use DatabaseMigrations;

     public function test_an_authenticated_user_may_participate_in_forum_threads()
     {
       // Given we have authenticated user and existing thread

       $user = factory('App\User')->create();
       $this->be($user);

       // When the user adds a reply to threads

       $thread = factory('App\Thread')->create();

       $reply = factory('App\Reply')->make();
       $this->post($thread->path().'/replies', $reply->toArray());

       //Then their reply should be visible on the page

       $this->get($thread->path())
          ->assertSee($reply->body);
     }
}
