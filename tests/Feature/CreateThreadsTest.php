<?php
namespace Tests\Feature;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CreateThreadsTest extends TestCase
{
    use DatabaseMigrations;

    function test_guest_cannot_create_threads()
    {

        $this->withExceptionHandling();

        $this->get('/threads/create')
            ->assertRedirect('/login');


        $this->post('/threads')
            ->assertRedirect('/login');

    }


    function test_an_authenticated_user_can_create_new_forum_threads()
    {
        $this->withExceptionHandling()->signIn();

        $thread = create('App\Thread');

        $response = $this->post('/threads', $thread->toArray());

        $this->get($response->headers->get('Location'))
            ->assertSee($thread->title)
            ->assertSee($thread->body);
    }

    public function test_a_thread_requires_a_title()
    {

        $this->publish_thread(['title' => null])
            ->AssertSessionHasErrors('title');
    }

    public function test_a_thread_requires_a_body()
    {

        $this->publish_thread(['body' => null])
            ->AssertSessionHasErrors('body');
    }

    public function test_a_thread_requires_a_valid_channel()
    {
        factory('App\Channel', 2)->create();

        $this->publish_thread(['channel_id' => null])
            ->AssertSessionHasErrors('channel_id');

        $this->publish_thread(['channel_id' => 999])
            ->AssertSessionHasErrors('channel_id');
    }

    public function publish_thread($overrides = [])
    {
        $this->withExceptionHandling()->signIn();

        $thread = make('App\Thread', $overrides);

        return $this->post('/threads', $thread->toArray());
    }
}
