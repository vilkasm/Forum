<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ThreadTest extends TestCase
{
    use DatabaseMigrations;

    protected $thread;

    public function setUp()
    {
         parent::setUp();
         $this->thread = factory('App\Thread')->create();
    }

    public function test_a_thread_has_replies()
    {
         $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->thread->replies);
    }

    public function test_a_thread_has_a_creator()
    {
      //Given that i have thread when i fetch thread owner it should be instance of user
          $this->assertInstanceOf('App\User', $this->thread->creator);
    }

    public function test_a_thread_can_add_a_reply()
    {
         $this->thread->addReply([
              'body' => 'replybody',
              'user_id' => 1
         ]);

         $this->assertCount(1, $this->thread->replies);
    }
}
